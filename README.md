# Web Development API Task

## Installation & Commands

Clone this repository and run

```bash
npm install
```

Run project in development mode

```bash
npm run serve
```

Build project

```bash
npm run build
```

Build and start project

```bash
npm run start
```

Run test cases

```bash
npm run test
```

Run with docker 
```bash
docker-compose up
```

## Configuration

In .env file please update 
* PORT (Default 5000)
* DB (MongoDB connection string)
* TEST_DB (MongoDB connection string for test database)
* NODE_ENV (dev / test)

## Technology Used

Following technologies / frameworks used in this project : 
* NodeJS
* Express
* Typescript
* MongoDB
* Jest 
* Swagger

## API Documentation

After running project, visit url :  
<BASE_URL>:<PORT>/api-docs   
For example : http://localhost:5000/api-docs  
  
You can see all available endpoints and can run directly from swagger