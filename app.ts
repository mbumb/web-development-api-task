import bodyParser from 'body-parser';
import express from 'express';
import dotenv from 'dotenv';
import { ConnectionOptions, connect } from "mongoose";
import user from "./routes/user";
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './docs/api.json';

/* Getting config variables from .env file */
dotenv.config();

/* Initializing express */
const app = express();
const db = (process.env.NODE_ENV == 'test') ? process.env.TEST_DB : process.env.DB;

/* Function to connect database */
const connectDB = async () => {
  try {
    const options: ConnectionOptions = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    };
    await connect(db, options);
    console.log("MongoDB Connected...");
  } catch (err) {
    console.error(err.message);
    // Exit process with failure
    process.exit(1);
  }
};

/* Connecting database */
connectDB();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/user',user);

/* Route for api documenation */
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

export default app