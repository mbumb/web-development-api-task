import { model, Schema } from "mongoose";

const userSchema: Schema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  givenName: {
    type: String,
    required: true
  },
  familyName: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  }
});

const User = model("User", userSchema);

export default User;