/* Defining user interface */
interface IUser {
    email: string;
    givenName: string;
    familyName: string;
}

export default IUser;