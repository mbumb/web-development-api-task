import User from '../models/user';
import IUser from '../interface/user';

/* 
    getUserList
    Get list of users
    @return user [object] 
*/
const getUsersList = async () => {
	try {
		let users: [IUser] = await User.find().exec();
		return { message: "Success", data: users };
	} catch (error) {
		return error;
	}
}

/* 
    getUser
    Get details of single user by id
    @params id
    @return user object 
*/
const getUser = async (id: string) => {
	try {
		let user: IUser = await User.findOne({ _id: id }).exec();
		if(user) {
			return { message: "Success", data: user };
		} else {
			return { message: "No user found" };
		}
		return user; 
	} catch (error) {
		return error;
	}
}

/* 
    createUser
    create new user
    @params email, givenName, familyName
    @return success / failure 
*/
const createUser = async (data: IUser) => {
	try {
		let user = await User.findOne({ "email": data.email }).exec();
		if(user) {
			return { message: "Email id already exists" };
		} else {
			await User.create(data);
			return { message: "User created successfully" };
		}
	} catch (error) {
		return error;
	}
}

/* 
    updateUser
    update details of an existing user by id
    @params id, email, givenName, familyName
    @return success / failure 
*/
const updateUser = async (id: string, data: IUser) => {
	try {
		let user = await User.findOne({ "email": data.email }).exec();
		if(!user || (user && user.id == id)) {
			let result = await User.updateOne({ "_id" : id }, { $set: data });	
			if(result) {
				return { message: "User updated successfully" }	
			} else {
				return { message: "Operation failed" }	
			}
		} else {
			return { message: "Email id already exists" };
		}
	} catch (error) {
		return error;
	}
}

/* 
    deleteUser
    delete user by id
    @params id
    @return success / failure 
*/
const deleteUser = async (id: string) => {
	try {
		let user = await User.deleteOne({ "_id" : id });
		if(user) {
			return { message: "User deleted successfully" }
		} else {
			return { message: "Operation failed" }
		}
	} catch (error) {
		return error;
	}
}

export default { getUsersList, getUser, createUser, updateUser, deleteUser }