import { Router } from 'express';
import controller from '../controllers/user';
import * as Joi from 'joi'
import { createValidator } from 'express-joi-validation';
import schema from '../validations/userSchema';
import sanitization from '../middleware/sanitization'

const validator = createValidator()

const router: Router = Router();


/* Defining routes with validations for user */
router.get(`/`, controller.getUsersList);
router.get(`/:id`, controller.getUser);
router.post(`/create`, validator.body(schema.user), sanitization, controller.createUser);
router.put(`/update/:id`, validator.params(schema.id), validator.body(schema.user), sanitization, controller.updateUser);
router.delete(`/delete/:id`, validator.params(schema.id), controller.deleteUser);

export default router;
