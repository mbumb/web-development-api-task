import * as Joi from 'joi'

const user = Joi.object({
  	email: Joi.string().email().required(),
  	givenName: Joi.string().required(),
  	familyName: Joi.string().required()
})

const id = Joi.object({
	id: Joi.string().required(),
})

export default { user, id };