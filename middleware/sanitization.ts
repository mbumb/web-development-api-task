import { Request, Response, NextFunction } from 'express';
import sanitize from 'mongo-sanitize';

const sanitization = async (req: Request, res: Response, next: NextFunction) => {
  req.body = sanitize(req.body);
  next();
}

export default sanitization;