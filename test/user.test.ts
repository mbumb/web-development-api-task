import supertest from 'supertest'

import app from '../app'
import User from '../models/user'
var user;

describe('Check APIs', () => {


    beforeAll( async () => {
        process.env.NODE_ENV = 'test';
        await User.remove({});
    })

    it('Create a new user with an invalid email id', async () => {
        const res = await supertest(app).post('/user/create').send({"email":"john.doe","givenName":"john","familyName":"doe"});
        expect(res.status).toBe(400)
    })

    it('Create a new user', async () => {
        const res = await supertest(app).post('/user/create').send({"email":"john.doe@test.com","givenName":"john","familyName":"doe"});
        user = await User.find({}).limit(1);
        expect(res.body.message).toBe('User created successfully')
    })

    it('Create a new user with an existing email id', async () => {
        const res = await supertest(app).post('/user/create').send({"email":"john.doe@test.com","givenName":"john","familyName":"doe"});
        expect(res.body.message).toBe('Email id already exists');
    })

    it('Request to get list of all users', async () => {
        const res = await supertest(app).get('/user')
        expect(res.status).toBe(200)
    })

    it('Length of data should be 1', async () => {
        const res = await supertest(app).get('/user')
        expect(res.body.data.length).toBe(1)
    })

    it('Get specific user details by id', async () => {
        const res = await supertest(app).get(`/user/${user[0]._id}`)
        expect(res.status).toBe(200)
    })

    it('Get specific user details by incorrect id', async () => {
        const res = await supertest(app).get(`/user/111111111111111111111111`)
        expect(res.body.message).toBe('No user found')
    })

    it('Get specific user details by incorrect id', async () => {
        const res = await supertest(app).get(`/user/${user[0]._id}`)
        expect(res.body.data.email).toBe('john.doe@test.com')
        expect(res.body.data.givenName).toBe('john')
        expect(res.body.data.familyName).toBe('doe')
    })

    it('Update user given name and email', async () => {
        const res = await supertest(app).put(`/user/update/${user[0]._id}`).send({"email":"jane.doe@test.com","givenName":"jane","familyName":"doe"})
        user = await User.find({'_id':user[0]._id}).limit(1);
        expect(user[0].givenName).toBe('jane')
        expect(user[0].email).toBe('jane.doe@test.com')
    })

    it('Update user with an extra parameter', async () => {
        const res = await supertest(app).put(`/user/update/${user[0]._id}`).send({"email":"jane.doe@test.com","givenName":"jane","familyName":"doe","city":"London"})
        expect(res.status).toBe(400)
    })

    it('Update user without required parameter', async () => {
        const res = await supertest(app).put(`/user/update/${user[0]._id}`).send({"email":"jane.doe@test.com","givenName":"jane"})
        expect(res.status).toBe(400)
    })

    it('Delete user', async () => {
        const res = await supertest(app).delete(`/user/delete/${user[0]._id}`)
        user = await User.find({});
        expect(user.length).toBe(0);
    })
})