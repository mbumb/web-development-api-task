import { Request, Response } from 'express';
import service from '../services/user';
import IUser from '../interface/user';

const success_code: number = 200;
const error_code: number = 500;

/* 
    getUser
    Get details of single user by id
    @params id
    @return user object 
*/
const getUser = async (req: Request, res: Response) => {
    try {
        let id: string = req.params.id;
        let result: IUser = await service.getUser(id);
        res.status(success_code).json(result);
    } catch (error) {
        res.status(error_code).json(error);
    }
}

/* 
    getUserList
    Get list of users
    @return user [object] 
*/
const getUsersList = async (req: Request, res: Response) => {
    try {
        let result: [IUser] = await service.getUsersList();
        res.status(success_code).json(result);
    } catch (error) {
        res.status(error_code).json(error);
    }
}

/* 
    createUser
    create new user
    @params email, givenName, familyName
    @return success / failure 
*/
const createUser = async (req: Request, res: Response) => {
    try {
        let data: IUser = req.body;
        let result = await service.createUser(data);
        res.status(success_code).json(result);
    } catch (error) {
        res.status(error_code).json(error);
    }
}

/* 
    updateUser
    update details of an existing user by id
    @params id, email, givenName, familyName
    @return success / failure 
*/
const updateUser = async (req: Request, res: Response) => {
    try {
        let id: string = req.params.id;
        let data: IUser = req.body;
        let result = await service.updateUser(id, data);
        res.status(success_code).json(result);
    } catch (error) {
        res.status(error_code).json(error);
    }
}

/* 
    deleteUser
    delete user by id
    @params id
    @return success / failure 
*/
const deleteUser = async (req: Request, res: Response) => {
    try {
        let id: string = req.params.id;
        
        let result = await service.deleteUser(id);
        res.status(success_code).json(result);
    } catch (error) {
        res.status(error_code).json(error);
    }
}

export default { getUser, getUsersList, createUser, updateUser, deleteUser }